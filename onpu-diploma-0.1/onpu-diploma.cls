\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{onpu-diploma}[2013/11/19 ONPU dimploma class file v.0.1]

% Version 0.1 supports 'kr' mode only 

%\ProvidesClass{minimal}[1995/10/30 Standard LaTeX minimal class]
%\renewcommand{\normalsize}{\fontsize{10pt}{12pt}\selectfont}
%\setlength{\textwidth}{6.5in}
%\setlength{\textheight}{8in}
%\pagenumbering{arabic}
% needed even though this class will
% not show page numbers

%%%%%%%%%%%%%%%%%%%%%%
% Processing options %
%%%%%%%%%%%%%%%%%%%%%%


% Document type
\newif\ifkr
\krtrue

\DeclareOption{kr}{
  \krtrue
}


% Document language

\newif\ifrus
\rusfalse

\DeclareOption{rus}{%
    \rustrue
}

\DeclareOption{ukr}{%
    \ifrus
      \PackageError{foo}{Options 'ukr' and 'rus' are mutually exclusive.}{}%
    \fi
    \rusfalse
}



% Male or female

\newif\ifstudentka
\studentkafalse
\DeclareOption{studentka}{%
    \studentkatrue
}



% Source text encoding

\newif\ifcp
\cpfalse

\DeclareOption{cp1251}{
    \cptrue
    %\PassOptionsToPackage{\CurrentOption}{inputenc}
}

\DeclareOption{utf8}{%
    \ifcp
        \PackageError{onpu-diploma}{Options 'utf8' and cp1251' are mutually exclusive.}{}%
    \else
        %\PackageWarning{onpu-diploma}{Using UTF-8 encoding.}{}%
        %\PassOptionsToPackage{\CurrentOption}{inputenc}
        \cpfalse
    \fi
}



% All unknown options will go here

\DeclareOption*{%
    \PackageWarning{onpu-diploma}{Unknown option: '\CurrentOption'}%
}

\ProcessOptions\relax





% package body:


% Our class is based on the standard 'article' slass
\LoadClass[14pt,a4paper]{article}

% Load 'inputenc' package with the stated encoding
% must be loaded BEFORE the 'babel' package
\ifcp
    \PackageInfo{onpu-diploma}{Using CP1251 encoding.}
    \PackageWarning{onpu-diploma}{Using CP1251 encoding.}
    %\PassOptionsToPackage{cp1251}{inputenc}
    \RequirePackage[cp1251]{inputenc}   % loading with the options stated 
\else
    \PackageWarning{onpu-diploma}{Using UTF-8 encoding.}{}%
    %\PassOptionsToPackage{utf8}{inputenc}
    \RequirePackage[utf8]{inputenc}   % loading with the options stated 
\fi


% Load the 'babel' package for the stated language
\ifrus
   % Russian Babel package
   \RequirePackage[russian]{babel}
\else
   % Ukrainian Babel package
   \RequirePackage[ukrainian]{babel}
\fi



%\RequirePackage{ucs}       % I don't know what this package actually does

\RequirePackage[T2A,T1]{fontenc}  % Setting this explicitly solves the issue with 
                                  % the \MakeUppercase command for Cyrillic text

\RequirePackage[sort&compress,square,numbers,comma]{natbib}
\RequirePackage{graphicx} % We definitely deed some pictures

%\RequirePackage[left=3cm,right=1cm,top=2cm,bottom=2cm]{geometry}

\RequirePackage{color} % needed only for this class testing


%\newcommand{\ruk}[1]{Text}         % Непонятно, что
\def\ruk#1{\def\@ruk{#1}}
%\newcommand{\ruk#1}{\def\@ruk{#1}} % Не пашет

\def\gruppa#1{\def\@gruppa{#1}}
\def\predmet#1{\def\@predmet{#1}}
\def\year#1{\def\@year{#1}}
\def\kurs#1{\def\@kurs{#1}}
\def\napravl#1{\def\@napravl{#1}}
\def\spec#1{\def\@spec{#1}}


% Define the default font parameters
\renewcommand{\normalsize}{\fontsize{14}{21}\fontfamily{cmr}\upshape\selectfont}



% Define a frontpage

\ifrus
    % Russian front page
    \renewcommand{\maketitle}{

    %\begin{titlepage}    % With titlepage it starts page numbering from the second page
    \thispagestyle{empty}
    \begin{center}
        \fontsize{11}{11}\upshape\bfseries\selectfont
        \ifcp        % CP1251 encoding
            �������� ������������ ������������ �����������\\
            ������� ���������� �� ���������������� ������ ������\\
        \else        % UTF8 encoding
            Одесский национальный политехнический университет\\
            Кафедра теоретической и экспериментальной ядерной физики\\
        \fi

        \vspace{5cm}

        \fontsize{18}{18}\fontfamily{cmr}\bfseries\upshape\selectfont
        \ifcp        % CP1251 encoding
            ������� ������
        \else        % UTF8 encoding
            КУРСОВАЯ РОБОТА
        \fi
    \end{center}
    \fontsize{14}{21}\selectfont
    \ifcp        % CP1251 encoding
        � ��������� <<\@predmet>> \\
        �� ����: <<\@title>>\\
    \else        % UTF8 encoding
        по дисциплине <<\@predmet>> \\
        на тему: <<\@title>>\\
    \fi

    \vspace{2cm}

    \hfill
    \begin{minipage}{8.5cm}

        \ifcp        % CP1251 encoding
            \ifstudentka
                ��������� \@kurs\ �����, ����� \@gruppa\\
            \else
                �������� \@kurs\ �����, ����� \@gruppa\\
            \fi
            ������� ��������� <<\@napravl>>\\
            ������������ <<\@spec>>\\
            \@author \\

            �������: \@ruk\\
            ����������� �����: \hrulefill\\
            ʳ������ ����: \hrulefill\\
            ������: ECTS \hrulefill\\

        \else        % UTF8 encoding
            \ifstudentka
                Студентки \@kurs\ курса, группы \@gruppa\\
            \else
                Студента \@kurs\ курса, группы \@gruppa\\
            \fi
                направления подготовки <<\@napravl>>\\
                специальности <<\@spec>>\\
                \@author \\

                Руководитель: \@ruk\\
                Национальная шкала: \hrulefill\\
                Количество баллов: \hrulefill\\
                Оценка: ECTS \hrulefill\\
        \fi
    \end{minipage}

    \vfill

    \fontsize{10}{10}\selectfont

    \ifcp        % CP1251 encoding
        \hfill ����� ���� \hspace{9cm}
    \else        % UTF8 encoding
        \hfill Члены комиссии \hspace{9cm}
    \fi

    \hfill
    \begin{minipage}{8.5cm}
        \underline{\hspace{3cm}} \hfill \underline{\hspace{4.5cm}}\\
        \begin{minipage}{3cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (�����)
                \else        % UTF8 encoding
                    (подпись)
                \fi
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{4.5cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (������� �� ��������)
                \else        % UTF8 encoding
                    (фамилия и инициалы)
                \fi
            \end{center}
        \end{minipage}
        \underline{\hspace{3cm}} \hfill \underline{\hspace{4.5cm}}\\
        \begin{minipage}{3cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (�����)
                \else        % UTF8 encoding
                    (подпись)
                \fi
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{4.5cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (������� �� ��������)
                \else        % UTF8 encoding
                    (фамилия и инициалы)
                \fi
            \end{center}
        \end{minipage}
        \underline{\hspace{3cm}} \hfill \underline{\hspace{4.5cm}}\\
        \begin{minipage}{3cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (�����)
                \else        % UTF8 encoding
                    (подпись)
                \fi
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{4.5cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (������� �� ��������)
                \else        % UTF8 encoding
                    (фамилия и инициалы)
                \fi
            \end{center}
        \end{minipage}
    \end{minipage}

    \vspace{2cm}

    \begin{center}
        \fontsize{10}{10}\selectfont
        \ifcp        % CP1251 encoding
            �.����� -- \@date\ ��
        \else        % UTF8 encoding
            г.Одесса -- \@date\ год
        \fi
    \end{center}

    %\end{titlepage}


    % Make contents page
    \newpage
    \normalsize
    \pagestyle{myheadings} 
    \tableofcontents
    \newpage
    }

\else
    % Ukrainian frontpage
    \renewcommand{\maketitle}{

    %\begin{titlepage}    % With titlepage it starts page numbering from the second page
    \thispagestyle{empty}
    \begin{center}
        \fontsize{11}{11}\upshape\bfseries\selectfont
        \ifcp        % CP1251 encoding
            �������� ������������ ������������ �����������\\
            ������� ���������� �� ���������������� ������ ������\\
        \else        % UTF8 encoding
            Одеський національній політехнічний університет\\
            Кафедра теоретичної та експериментальної ядерної фізики\\
        \fi

        \vspace{5cm}

        \fontsize{18}{18}\fontfamily{cmr}\bfseries\upshape\selectfont
        \ifcp        % CP1251 encoding
            ������� ������
        \else        % UTF8 encoding
            КУРСОВА РОБОТА
        \fi
    \end{center}
    \fontsize{14}{21}\selectfont
    \ifcp        % CP1251 encoding
        � ��������� <<\@predmet>> \\
        �� ����: <<\@title>>\\
    \else        % UTF8 encoding
        з дисципліни <<\@predmet>> \\
        на тему: <<\@title>>\\
    \fi

    \vspace{2cm}

    \hfill
    \begin{minipage}{8cm}

        \ifcp        % CP1251 encoding
            \ifstudentka
                ��������� \@kurs\ �����, ����� \@gruppa\\
            \else
                �������� \@kurs\ �����, ����� \@gruppa\\
            \fi
            ������� ��������� <<\@napravl>>\\
            ������������ <<\@spec>>\\
            \@author \\

            �������: \@ruk\\
            ����������� �����: \hrulefill\\
            ʳ������ ����: \hrulefill\\
            ������: ECTS \hrulefill\\

        \else        % UTF8 encoding
            \ifstudentka
                Студентки \@kurs\ курсу, групи \@gruppa\\
            \else
                Студента \@kurs\ курсу, групи \@gruppa\\
            \fi
                напряму підготовки <<\@napravl>>\\
                спеціальності <<\@spec>>\\
                \@author \\

                Керівник: \@ruk\\
                Національна шкала: \hrulefill\\
                Кількість балів: \hrulefill\\
                Оцінка: ECTS \hrulefill\\
        \fi
    \end{minipage}

    \vfill

    \fontsize{10}{10}\selectfont

    \ifcp        % CP1251 encoding
        \hfill ����� ���� \hspace{9cm}
    \else        % UTF8 encoding
        \hfill Члени комісії \hspace{9cm}
    \fi

    \hfill
    \begin{minipage}{8cm}
        \underline{\hspace{3cm}} \hfill \underline{\hspace{4.5cm}}\\
        \begin{minipage}{3cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (�����)
                \else        % UTF8 encoding
                    (підпис)
                \fi
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{4.5cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (������� �� ��������)
                \else        % UTF8 encoding
                    (прізвище та ініціали)
                \fi
            \end{center}
        \end{minipage}
        \underline{\hspace{3cm}} \hfill \underline{\hspace{4.5cm}}\\
        \begin{minipage}{3cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (�����)
                \else        % UTF8 encoding
                    (підпис)
                \fi
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{4.5cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (������� �� ��������)
                \else        % UTF8 encoding
                    (прізвище та ініціали)
                \fi
            \end{center}
        \end{minipage}
        \underline{\hspace{3cm}} \hfill \underline{\hspace{4.5cm}}\\
        \begin{minipage}{3cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (�����)
                \else        % UTF8 encoding
                    (підпис)
                \fi
            \end{center}
        \end{minipage}
        \hfill
        \begin{minipage}{4.5cm}
            \fontsize{8}{8}\selectfont
            % subscriptsize?
            \begin{center}
                \ifcp        % CP1251 encoding
                    (������� �� ��������)
                \else        % UTF8 encoding
                    (прізвище та ініціали)
                \fi
            \end{center}
        \end{minipage}
    \end{minipage}

    \vspace{2cm}

    \begin{center}
        \fontsize{10}{10}\selectfont
        \ifcp        % CP1251 encoding
            �.����� -- \@date\ ��
        \else        % UTF8 encoding
            м.Одеса -- \@date\ рік
        \fi
    \end{center}

    %\end{titlepage}


    % Make contents page
    \newpage
    \normalsize
    \pagestyle{myheadings} 
    \tableofcontents
    \newpage
    }
\fi



% Points after section and subsection numbers
\renewcommand{\thesection}{\arabic{section}.}
\renewcommand{\thesubsection}{\thesection\arabic{subsection}.}

\renewcommand{\section}{%
\newpage
\suppressfloats[t]
\@startsection
{section}{1}{0pt}{14pt}%
{14pt}
{\normalsize\rmfamily\upshape\bfseries\centering\MakeUppercase}%
%{\bfseries\uppercase}%
%\@startsection{<name>}{<level>}{<indent>}{<beforeskip>}{<afterskip>}{<style>}*[<altheading>]{<heading>}
}





\renewcommand{\subsection}{%
\@startsection
{subsection}{2}{0pt}{14pt}%
{14pt}
{\normalsize\rmfamily\upshape\bfseries\centering}%
}

\setcounter{secnumdepth}{2}  % We have only sections and subsections



% Table of contents customizations

% Solution from http://tex.stackexchange.com/a/137212/16578
%
%\let\stdl@section\l@section
%\renewcommand*{\l@section}[2]{%
%  \stdl@section{\MakeUppercase{#1}}{\textcolor{red}{#2}}}


% Definition from /usr/share/texlive/texmf-dist/tex/latex/base/article.cls
%
%\newcommand*\l@section[2]{%
%  \ifnum \c@tocdepth >\z@
%    \addpenalty\@secpenalty
%    \addvspace{1.0em \@plus\p@}%
%    \setlength\@tempdima{1.5em}%
%    \begingroup
%      \parindent \z@ \rightskip \@pnumwidth
%      \parfillskip -\@pnumwidth
%      \leavevmode \bfseries
%      \advance\leftskip\@tempdima
%      \hskip -\leftskip
%      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
%    \endgroup
%  \fi}




% !!! CONFLICTS WITH HYPERREF PACKAGE !!!
% see http://tex.stackexchange.com/a/137212/16578 for details
%
\renewcommand*{\l@section}[2]{\@dottedtocline{1}{0em}{1.5em}{\MakeUppercase{#1}}{#2}}
% !!! CONFLICTS WITH HYPERREF PACKAGE !!!



%\patchcmd{\l@section}{#1}{\MakeUppercase{#1}}{}{}% Sections use UPPERCASE in ToC

%\renewcommand*{\l@section}[2]{%
%     {\bfseries \MakeUppercase{#1}}\nobreak
%     {#2}}%



% Setting page layout
%
% Physical paper size
%\setlength{\paperheight}{29.7cm}
%\setlength{\paperwidth}{21cm}

% Horizontal parameters
%\setlength{\hoffset}{0.5cm}       % for the resulting offset 1in + 0.5cm = 3cm
\setlength{\hoffset}{-1in}         % for the resulting total offset = 0
\addtolength{\hoffset}{2.5cm}        % 2.5 cm on the left
\setlength{\oddsidemargin}{0pt}
\setlength{\marginparsep}{0cm}
\setlength{\marginparwidth}{0cm} 
\setlength{\textwidth}{17.5cm}    % A4 paper width minus 4cm

% Vertical parameters
\setlength{\voffset}{-0.5cm}      % for the resulting offset 1in - 0.5cm = 2cm
\setlength{\topmargin}{0cm}
\setlength{\headheight}{14pt}     % = шрифту верхнего колонтитула
\setlength{\headsep}{21pt}        % полуторный интервал
\setlength{\textheight}{25cm}
\setlength{\footskip}{0cm}

\setlength{\marginparpush}{0cm}


% Figures, tables and equations numbering format (1.1), (1.2), (2.1), ...
\renewcommand{\thefigure}{\arabic{section}.\arabic{figure}}
\renewcommand{\thetable}{\arabic{section}.\arabic{table}}
\renewcommand{\theequation}{\arabic{section}.\arabic{equation}}


% Change the column in the Figure and Table captions to a dot
%\renewcommand{\makecaption}[2]{
%    #1 . #2
%}

\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \hb@xt@\hsize{\hfil #1. #2 \hfil}%
  \vskip\belowcaptionskip}



% Bibliography customizations
    \renewcommand{\bibnumfmt}[1]{#1.\hfill} % нумерация источников в самом списке - через точку
    %\renewcommand{\bibsection}{\likechapter{Список литературы}} % заголовок специального раздела
    \bibliographystyle{unsrtnat}  % Not sure if it is a good place for this
%    \bibpunct{}{}{,}{n}{,}{,}




% Executed after \begin{document}
\AtBeginDocument{

\ifcp        % CP1251 encoding
    \ifrus
        % Russian
        \renewcommand\refname{����������}  % Rename references section
    \else
        % Ukrainian
        \renewcommand\refname{˳��������}  % Rename references section
    \fi
\else        % UTF8 encoding
    \ifrus
        % Russian
        \renewcommand\refname{Литература}  % Rename references section
    \else
        % Ukrainian
        \renewcommand\refname{Література}  % Rename references section
    \fi
\fi

}

% Executed before \end{document}
\AtEndDocument{

\ifcp        % CP1251 encoding
    \ifrus
        % Russian
        \addcontentsline{toc}{section}{����������}  % Add a line for References to the ToC
    \else
        % Ukrainian
        \addcontentsline{toc}{section}{˳��������}  % Add a line for References to the ToC
    \fi
\else        % UTF8 encoding
    \ifrus
        % Russian
        \addcontentsline{toc}{section}{Литература}  % Add a line for References to the ToC
    \else
        % Ukrainian 
        \addcontentsline{toc}{section}{Література}  % Add a line for References to the ToC
    \fi
\fi
}